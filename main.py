import argparse

import utils


def draw(input_filename: str, output_filename: str):
    print('Start drawing.')
    lines = utils.read_file(input_filename)
    utils.clean_output_file(filename=output_filename)
    utils.parse_file_lines(lines, filename=output_filename)
    print('Finish drawing.')


def main():
    parser = argparse.ArgumentParser(description='Console Draw Tool')
    parser.add_argument('input_filename', type=str, help="input file name.")
    parser.add_argument('--output_filename', type=str, help="ountput file name. Default - 'samples/output.txt'",
                        default='samples/output.txt')
    args = parser.parse_args()
    draw(input_filename=args.input_filename, output_filename=args.output_filename)


if __name__ == '__main__':
    main()

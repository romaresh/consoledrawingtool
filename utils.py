import os
from pathlib import Path

from constants import PROJECT_ROOT
from shapes import Point, Rectangle, Line
from canvas import Canvas


def read_file(filename: str) -> list:
    """
    Read non empty lines from input file.
    """
    file = get_existing_file(filename=filename)
    if file is None:
        raise Exception("Input file doesn't exist")
    with open(file) as file:
        # skip empty lines
        lines = filter(None, (line.rstrip() for line in file.readlines()))
    return lines


def write_to_file(filename: str):
    """
    Append new canvas to file.
    """
    filepath = os.path.join(PROJECT_ROOT, filename)
    with open(filepath, 'a') as file:
        file.write(str(Canvas()))


def clean_output_file(filename: str):
    """
    Clean existing output file.
    """
    file = get_existing_file(filename=filename)
    if file:
        open(file, 'w').close()


def get_existing_file(filename: str) -> Path:
    """
    Check file existing anf if it exist return path to this file.
    """
    file = Path(os.path.join(PROJECT_ROOT, filename))
    return file if file.is_file() else None


def parse_file_lines(lines: list, filename: str):
    """
    Parse lines from file. Separate shapes letters from attributes.
    """
    for line_number, line in enumerate(lines):
        command, params = line.split(maxsplit=1)
        params = params.strip().split()
        # check if first command is draw canvas
        if line_number == 0:
            if command == 'C':
                # create canvas, Canvas is a singletone so now we can just use Canvas() for getting existing instance
                Canvas(*params)
                write_to_file(filename=filename)
                continue
            raise Exception("First command should be 'C'.")
        parse_commands(command, params, filename)


def parse_commands(command: str, params: list, filename: str):
    """
    Mapping letters from file to shapes, create shapes with given params
    and write shapes to canvas
    """
    if command == 'L':
        line = Line(Point(*params[0:2]), Point(*params[2:4]))
        Canvas().draw_line(line)
    elif command == 'R':
        rectangle = Rectangle(Point(*params[0:2]), Point(*params[2:4]))
        Canvas().draw_rectangle(rectangle)
    elif command == 'B':
        Canvas().bucket_fill(Point(*params[:2]), params[2])
    else:
        raise Exception(f'Not valid command: {command}')
    write_to_file(filename)

from helpers import ObjectIterator


#
# Mixins
#
class OnlyStraightLineMixin:
    def __new__(cls, point1: 'Point', point2: 'Point'):
        if point1.x == point2.x or point1.y == point2.y:
            return super().__new__(cls)
        raise Exception('Only straight line available.')


class ValidateRectangleMixin:
    def __new__(cls, point1: 'Point', point2: 'Point'):
        # Check if rectangle is not line
        if point1.x != point2.x and point1.y != point2.y:
            # Check if point1 is left upper point
            if point1.x < point2.x and point1.y < point2.y:
                return super().__new__(cls)
        raise AssertionError('Not valid points for rectangle.')


#
# Shapes
#
class Point:
    def __init__(self, x: str, y: str):
        self.x = abs(int(x))
        self.y = abs(int(y))

    def get_neighbors(self) -> tuple:
        """
        Return tuple with all neighbors points for current point.
        """
        p1 = Point(self.x - 1, self.y - 1)
        p2 = Point(self.x, self.y - 1)
        p3 = Point(self.x + 1, self.y - 1)
        p4 = Point(self.x - 1, self.y)
        p9 = Point(self.x, self.y)
        p5 = Point(self.x + 1, self.y)
        p6 = Point(self.x - 1, self.y + 1)
        p7 = Point(self.x, self.y + 1)
        p8 = Point(self.x + 1, self.y + 1)
        return p1, p2, p3, p4, p5, p6, p7, p8, p9

    def __str__(self) -> str:
        return f"Point(x={self.x}, y={self.y})"

    def __eq__(self, other: 'Point') -> bool:
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def __hash__(self):
        return hash((self.x, self.y))


class Line(OnlyStraightLineMixin):
    def __init__(self, point1: Point, point2: Point):
        self.point1 = point1
        self.point2 = point2
        self.is_vertical = self._get_direction()
        self.points = self._get_points()

    def _get_direction(self) -> bool:
        is_vertical = False
        if self.point1.x == self.point2.x:
            is_vertical = True
        return is_vertical

    def _get_points(self) -> list:
        """
        Find all points which current line contains
        """
        points = []
        if self.is_vertical:
            distance = self._get_line_len(self.point1.y, self.point2.y)
            for y in distance:
                points.append(Point(self.point1.x, y))
        else:
            distance = self._get_line_len(self.point1.x, self.point2.x)
            for x in distance:
                points.append(Point(x, self.point1.y))
        return points

    @staticmethod
    def _get_line_len(coordinate1: int, coordinate2: int) -> range:
        max_value, min_value = max(coordinate1, coordinate2), min(coordinate1, coordinate2)
        return range(min_value, max_value + 1)

    def __iter__(self):
        return ObjectIterator(self.points)

    def __str__(self):
        return f"Line({self.point1}, {self.point2})"


class Rectangle(ValidateRectangleMixin):
    def __init__(self, point1: Point, point2: Point):
        self.point_top_left = point1
        self.point_bottom_right = point2
        self.point_bottom_left = Point(point1.x, point2.y)
        self.point_top_right = Point(point2.x, point1.y)
        self.lines = self._get_lines()

    def _get_lines(self):
        top = Line(self.point_top_left, self.point_top_right)
        bottom = Line(self.point_bottom_left, self.point_bottom_right)
        right = Line(self.point_top_right, self.point_bottom_right)
        left = Line(self.point_top_left, self.point_bottom_left)
        return top, right, bottom, left

    def __iter__(self):
        return ObjectIterator(self.lines)

    def __str__(self):
        return f"Rectangle({self.point_top_left}, {self.point_bottom_right})"

from helpers import Singleton, timeit
from shapes import Point, Rectangle, Line


class Canvas(metaclass=Singleton):
    VERTICAL_BORDER = '|'
    HORIZONTAL_BORDER = '-'
    FILLING_VALUE = ' '
    POINT_VALUE = 'x'

    def __init__(self, width: str, height: str):
        self.width = int(width) + 2
        self.height = int(height) + 2
        self.matrix = [[self.FILLING_VALUE for _ in range(self.width)] for _ in range(self.height)]
        self._draw_borders()

    def _draw_borders(self):
        for i in range(self.height):
            for j in range(self.width):
                if i == 0 or i == self.height - 1:
                    self.matrix[i][j] = self.HORIZONTAL_BORDER
                elif j == 0 or j == self.width - 1:
                    self.matrix[i][j] = self.VERTICAL_BORDER

    def _validate_point(self, point: Point, denied_symbols: tuple):
        # check if point inside canvas
        if point.x <= (self.width - 2) and point.y <= (self.height - 2):
            matrix_point = self.matrix[point.y][point.x]
            # check if point not on border
            if matrix_point not in denied_symbols:
                return
        raise Exception(f'Invalid position of point: {point}')

    def draw_point(self, point: Point):
        self._validate_point(point, (self.VERTICAL_BORDER, self.HORIZONTAL_BORDER))
        self.matrix[point.y][point.x] = self.POINT_VALUE

    def draw_line(self, line: Line):
        for point in line:
            self.draw_point(point)

    def draw_rectangle(self, rectangle: Rectangle):
        for line in rectangle:
            self.draw_line(line)

    @timeit
    def bucket_fill(self, point: Point, color: str):
        self._validate_point(point, (self.VERTICAL_BORDER, self.HORIZONTAL_BORDER, self.POINT_VALUE))
        obstacles = [self.VERTICAL_BORDER, self.HORIZONTAL_BORDER, self.POINT_VALUE, color]
        checking_points = [point]
        for current in checking_points:
            for next in current.get_neighbors():
                if self.matrix[next.y][next.x] not in obstacles:
                    self.matrix[next.y][next.x] = color
                    checking_points.append(next)

    def __str__(self) -> str:
        matrix = []
        for i in range(self.height):
            matrix.append(''.join(self.matrix[i]))
        return '\n'.join(matrix) + '\n'

import time
from typing import Callable


#
# Metaclasses
#
class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


#
# Iterators
#
class ObjectIterator:
    def __init__(self, objects: list):
        self.i = 0
        self.objects = objects
        self.n = len(objects)

    def __iter__(self):
        return self

    def __next__(self):
        if self.i < self.n:
            i = self.i
            self.i += 1
            return self.objects[i]
        else:
            raise StopIteration()


#
# Decorators
#
def timeit(method: Callable):
    def timed(*args, **kwargs):
        time_start = time.time()
        result = method(*args, **kwargs)
        time_end = time.time()
        print(f'Execution time for {method.__name__}(): {((time_end - time_start) * 1000):.4f} ms')
        return result

    return timed
